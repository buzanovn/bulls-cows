/*
 * Copyright (c) 2017. All rights reserved.
 */

package bullscows.controller;

import bullscows.Util;
import bullscows.controller.base.BasePlayerController;
import bullscows.model.BullsCows;
import bullscows.model.GuessResult;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class TwoPlayerController extends BasePlayerController {
    private int flag;
    private BullsCows user1BC, user2BC;
    private boolean user1Turn, user2Turn;
    private int user1_turns, user2_turns;

    @FXML
    private Label user1ImaginedNumber, user2ImaginedNumber, labelGuess1, labelGuess2, labelErrorMessage;
    @FXML
    private TextField textFieldGuess1, textFieldGuess2;
    @FXML
    private Button buttonSubmitGuess1, buttonSubmitGuess2;
    @FXML
    private TableView<GuessResult> tableView1, tableView2;
    @FXML
    private TableColumn<GuessResult, String> variantColumn1, variantColumn2, bcColumn1, bcColumn2;

    private ObservableList<GuessResult> user1GuessVariantsTable, user2GuessVariantsTable;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);

    }

    @Override
    public void beginTheGame() {
        flag = 5;
        numberEnterDialog(1);
        numberEnterDialog(2);
        user1ImaginedNumber.setText("Игрок 1");
        user2ImaginedNumber.setText("Игрок 2");
        Controller.isGamePlayingNow = true;
        user1Turn = true;
        user2Turn = false;
        isForSTurn();
        user1GuessVariantsTable = FXCollections.observableArrayList();
        user2GuessVariantsTable = FXCollections.observableArrayList();
        variantColumn1.setCellValueFactory(new PropertyValueFactory<>("guessNum"));
        bcColumn1.setCellValueFactory(new PropertyValueFactory<>("guessResult"));
        variantColumn2.setCellValueFactory(new PropertyValueFactory<>("guessNum"));
        bcColumn2.setCellValueFactory(new PropertyValueFactory<>("guessResult"));
        tableView1.setItems(user1GuessVariantsTable);
        tableView2.setItems(user2GuessVariantsTable);
        while (flag < 0) ;
    }

    public void numberEnterDialog(int n) {
        Dialog<Integer> dialog = new Dialog<>();
        dialog.setTitle("Загадайте число");
        dialog.setHeaderText("Игрок " + n + " загадайте ваше число");
        ButtonType loginButtonType = new ButtonType("Принять", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));
        TextField userNumberField = new TextField();
        userNumberField.setPromptText("1234 (число без повторений цифр)");
        grid.add(userNumberField, 1, 0);
        dialog.getDialogPane().setContent(grid);
        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return new Integer(userNumberField.getText());
            }
            return null;
        });
        Optional<Integer> result = dialog.showAndWait();
        result.ifPresent(str -> {
            if (Util.checkNumberMatchesCorrect(str.toString()))
                switch (n) {
                    case (1):
                        user1BC = new BullsCows(str);
                        break;
                    case (2):
                        user2BC = new BullsCows(str);
                        break;
                }

            else {
                Alert alert0 = new Alert(Alert.AlertType.ERROR);
                alert0.setTitle("Ошибка");
                alert0.setHeaderText("Не верно введено число!");
                alert0.setContentText("Нажмите \"Ок\", чтобы ввести число ещё раз, для выхода нажмите \"Выход\" ");
                Optional<ButtonType> result1 = alert0.showAndWait();
                if (result1.get() == ButtonType.OK)
                    numberEnterDialog(n);
                else
                    alert0.close();

            }
        });
    }

    public void isForSTurn() {
        textFieldGuess1.setVisible(user1Turn);
        labelGuess1.setVisible(user1Turn);
        buttonSubmitGuess1.setVisible(user1Turn);
        user1ImaginedNumber.setVisible(true);

        textFieldGuess2.setVisible(user2Turn);
        labelGuess2.setVisible(user2Turn);
        buttonSubmitGuess2.setVisible(user2Turn);
        user2ImaginedNumber.setVisible(true);

    }

    public void buttonSubmitGuessHandler1(ActionEvent actionEvent) {
        //textFieldGuess1.setText("");
        Platform.runLater(() -> textFieldGuess1.requestFocus());
        String str = textFieldGuess1.getText();
        if (user1_turns > 30) {
            flag = 1;
            Alert alertUserOver = new Alert(Alert.AlertType.INFORMATION);
            alertUserOver.setHeaderText("У вас закончились ходы, игрок №1!");
            alertUserOver.setContentText("К сожалению у вас закончились возможные ходы, хотите начать новую игру?");
            ButtonType OK = new ButtonType("Начать!");
            ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertUserOver.getButtonTypes().setAll(OK, Cancel);
            Optional<ButtonType> result = alertUserOver.showAndWait();
            if (result.get() == OK)
                beginTheGame();
            else
                exitToMenu();
            return;
        }
        if (user2_turns > 30) {
            flag = 2;
            Alert alertCompOver = new Alert(Alert.AlertType.INFORMATION);
            alertCompOver.setHeaderText("У игрока №2 закончились ходы!");
            alertCompOver.setContentText("К сожалению у компьютера закончились возможные ходы, хотите начать новую игру?");
            ButtonType OK = new ButtonType("Начать!");
            ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertCompOver.getButtonTypes().setAll(OK, Cancel);
            Optional<ButtonType> result = alertCompOver.showAndWait();
            if (result.get() == OK)
                beginTheGame();
            else
                exitToMenu();
            return;
        }
        if (Util.checkNumberMatchesCorrect(str)) {
            user1GuessVariantsTable.add(new GuessResult(str, user2BC.toString(new BullsCows(str))));
            BullsCows bc_temp = new BullsCows(str);
            if (user2BC.getBullsCount(bc_temp) == 4) {
                flag = 0;
                Alert alertUserWon = new Alert(Alert.AlertType.INFORMATION);
                alertUserWon.setHeaderText("Победа!");
                alertUserWon.setContentText("Вы отгадали число соперника и победили! Хотите начать новую игру или выйти?");
                ButtonType OK = new ButtonType("Начать!");
                ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
                alertUserWon.getButtonTypes().setAll(OK, Cancel);
                Optional<ButtonType> result = alertUserWon.showAndWait();
                if (result.get() == OK)
                    beginTheGame();
                else
                    exitToMenu();
                return;
            } else {
                user1_turns++;
                user1Turn = false;
                user2Turn = true;
                isForSTurn();
                /*Действие компьютера*/
                flag = -2;
                return;
            }
        } else {
            labelErrorMessage.setText("Вы ввели недопустимые символы!");
            labelErrorMessage.setTextFill(Color.RED);
        }
        flag = -1;
    }

    public void buttonSubmitGuessHandler2(ActionEvent actionEvent) {
        //textFieldGuess2.setText("");
        Platform.runLater(() -> textFieldGuess2.requestFocus());
        String str = textFieldGuess2.getText();
        if (user2_turns > 30) {
            flag = 1;
            Alert alertUserOver = new Alert(Alert.AlertType.INFORMATION);
            alertUserOver.setHeaderText("У вас закончились ходы, игрок №2!");
            alertUserOver.setContentText("К сожалению у вас закончились возможные ходы, хотите начать новую игру?");
            ButtonType OK = new ButtonType("Начать!");
            ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertUserOver.getButtonTypes().setAll(OK, Cancel);
            Optional<ButtonType> result = alertUserOver.showAndWait();
            if (result.get() == OK)
                beginTheGame();
            else
                exitToMenu();
            return;
        }
        if (user1_turns > 30) {
            flag = 2;
            Alert alertCompOver = new Alert(Alert.AlertType.INFORMATION);
            alertCompOver.setHeaderText("У игрока №1 закончились ходы!");
            alertCompOver.setContentText("К сожалению у игрока №1 закончились возможные ходы, хотите начать новую игру?");
            ButtonType OK = new ButtonType("Начать!");
            ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertCompOver.getButtonTypes().setAll(OK, Cancel);
            Optional<ButtonType> result = alertCompOver.showAndWait();
            if (result.get() == OK)
                beginTheGame();
            else
                exitToMenu();
            return;
        }
        if (Util.checkNumberMatchesCorrect(str)) {
            user2GuessVariantsTable.add(new GuessResult(str, user1BC.toString(new BullsCows(str))));
            BullsCows bc_temp = new BullsCows(str);
            if (user1BC.getBullsCount(bc_temp) == 4) {
                flag = 0;
                Alert alertUserWon = new Alert(Alert.AlertType.INFORMATION);
                alertUserWon.setHeaderText("Победа!");
                alertUserWon.setContentText("Вы отгадали число соперника и победили! Хотите начать новую игру или выйти?");
                ButtonType OK = new ButtonType("Начать!");
                ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
                alertUserWon.getButtonTypes().setAll(OK, Cancel);
                Optional<ButtonType> result = alertUserWon.showAndWait();
                if (result.get() == OK)
                    beginTheGame();
                else
                    exitToMenu();
                return;
            } else {
                user2_turns++;
                user2Turn = false;
                user1Turn = true;
                isForSTurn();
                flag = -2;
                return;
            }
        } else {
            labelErrorMessage.setText("Вы ввели недопустимые символы!");
            labelErrorMessage.setTextFill(Color.RED);
        }
        flag = -1;
    }
}
