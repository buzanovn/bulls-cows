package bullscows.controller;

import bullscows.Resources;
import bullscows.Util;
import bullscows.controller.base.BasePlayerController;
import bullscows.model.BullsCows;
import bullscows.model.GuessResult;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class OnePlayerController extends BasePlayerController implements Initializable {
    private boolean playerTurn;
    private BullsCows playersBC;
    private BullsCows compBC;
    int user_turns = 0, comp_turns = 0;
    int flag;
    @FXML
    private Label userImaginedNumber;
    @FXML
    private TextField textFieldGuess;
    @FXML
    private Label labelGuess;
    @FXML
    private Button buttonSubmitGuess;
    @FXML
    private TableView<GuessResult> tableView;
    @FXML
    private TableColumn<GuessResult, String> variantColumn;
    @FXML
    private TableColumn<GuessResult, String> bcColumn;
    @FXML
    private Label labelErrorMessage;

    private ObservableList<GuessResult> userGuessVariantsTable;

    @Override
    public void beginTheGame(){
        compBC = new BullsCows();
        flag = 5;
        numberEnterDialog();
        userImaginedNumber.setText(playersBC.getBullsCows());
        Controller.isGamePlayingNow = true;
        playerTurn = true;
        isCompOrUserTurn();
        userGuessVariantsTable = FXCollections.observableArrayList();
        variantColumn.setCellValueFactory(new PropertyValueFactory<>("guessNum"));
        bcColumn.setCellValueFactory(new PropertyValueFactory<>("guessResult"));
        tableView.setItems(userGuessVariantsTable);
        while(flag < 0);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        super.initialize(location, resources);
    }

    public void numberEnterDialog(){
        Dialog<String> dialog = new Dialog<>();
        dialog.setTitle("Загадайте число");
        dialog.setHeaderText(null);

        ButtonType loginButtonType = new ButtonType("Принять", ButtonBar.ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField userNumberField = new TextField();
        userNumberField.setPromptText("1234 (число без повторений цифр)");
        grid.add(userNumberField, 1, 0);
        dialog.getDialogPane().setContent(grid);

        dialog.setResultConverter(dialogButton -> {
            if (dialogButton == loginButtonType) {
                return userNumberField.getText();
            }
            return null;
        });

        Optional<String> result = dialog.showAndWait();

        result.ifPresent(resultingString -> {
            if (Util.checkNumberMatchesCorrect(resultingString))
                playersBC = new BullsCows(resultingString);
            else {
               alertNumberEnterError();
            }
        });
    }

    private void alertNumberEnterError() {
        Alert alertNumberEnterError = new Alert(Alert.AlertType.ERROR);
        alertNumberEnterError.setTitle("Ошибка");
        alertNumberEnterError.setHeaderText("Не верно введено число!");
        alertNumberEnterError.setContentText("Нажмите \"Ок\", чтобы ввести число ещё раз, для выхода нажмите \"Выход\" ");
        Optional<ButtonType> result1 = alertNumberEnterError.showAndWait();
        if(result1.get() == ButtonType.OK)
            numberEnterDialog();
        else
            alertNumberEnterError.close();
    }

    public void buttonSubmitGuessHandler() {
        System.out.println(compBC.getBullsCows());
        String str = textFieldGuess.getText();
        if (user_turns > 30){
            flag = 1;
            Alert alertUserOver = new Alert(Alert.AlertType.INFORMATION);
            alertUserOver.setHeaderText("У вас закончились ходы!");
            alertUserOver.setContentText("К сожалению у вас закончились возможные ходы, хотите начать новую игру?");
            ButtonType OK = new ButtonType("Начать!");
            ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertUserOver.getButtonTypes().setAll(OK,Cancel);
            Optional<ButtonType> result = alertUserOver.showAndWait();
            if(result.get() == OK)
                beginTheGame();
            else
                exitToMenu();
            return;
        }
        if (comp_turns > 30) {
            flag = 2;
            Alert alertCompOver = new Alert(Alert.AlertType.INFORMATION);
            alertCompOver.setHeaderText("У компьютера закончились ходы!");
            alertCompOver.setContentText("К сожалению у компьютера закончились возможные ходы, хотите начать новую игру?");
            ButtonType OK = new ButtonType("Начать!");
            ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertCompOver.getButtonTypes().setAll(OK,Cancel);
            Optional<ButtonType> result = alertCompOver.showAndWait();
            if(result.get() == OK)
                beginTheGame();
            else
                exitToMenu();
            return;
        }
        if (Util.checkNumberMatchesCorrect(str)) {
            userGuessVariantsTable.add(new GuessResult(str, compBC.toString(new BullsCows(str))));
            BullsCows bc_temp = new BullsCows(str);
            if (compBC.getBullsCount(bc_temp) == 4){
                flag = 0;
                Alert alertUserWon = new Alert(Alert.AlertType.INFORMATION);
                alertUserWon.setHeaderText("Победа!");
                alertUserWon.setContentText("Вы отгадали число компьюетра и победили! Хотите начать новую игру или выйти?");
                ButtonType OK = new ButtonType("Начать!");
                ButtonType Cancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);
                alertUserWon.getButtonTypes().setAll(OK,Cancel);
                Optional<ButtonType> result = alertUserWon.showAndWait();
                return;
            }
            else {
                    user_turns++;
                    playerTurn = false;
                    isCompOrUserTurn();
                /*Действие компьютера*/
                    comp_turns++;
                    playerTurn = true;
                isCompOrUserTurn();
                flag = -2;
                return;
            }
        } else {
            labelErrorMessage.setText("Вы ввели недопустимые символы!");
            labelErrorMessage.setTextFill(Color.RED);
           }
        flag = -1;
    }

    public void isCompOrUserTurn() {
        textFieldGuess.setVisible(playerTurn);
        labelGuess.setVisible(playerTurn);
        buttonSubmitGuess.setVisible(playerTurn);
        labelErrorMessage.setVisible(playerTurn);
    }
}
