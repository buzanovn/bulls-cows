package bullscows.controller.base;

import bullscows.Resources;
import bullscows.controller.Controller;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public abstract class BasePlayerController implements Initializable {
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        beginTheGame();
    }

    public abstract void beginTheGame();

    public static void exitToMenu(){
        Controller.isGamePlayingNow = false;
        try {
            Controller.primaryStage.setWidth(Resources.ScreenSizes.MAIN_WIDTH);
            Controller.primaryStage.setHeight(Resources.ScreenSizes.MAIN_HEIGHT);
            Controller.primaryStage.setScene(new Scene(new FXMLLoader(new File(Resources.FXML.MAIN).toURI().toURL()).load()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
