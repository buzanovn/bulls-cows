package bullscows.controller;

import bullscows.Resources;
import bullscows.controller.base.BasePlayerController;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    private static final double ONE_PLAYER_SCREEN_WIDTH = 360.0;
    private static final double ONE_PLAYER_SCREEN_HEIGHT = 450.0;

    public static boolean isGamePlayingNow;
    public static Stage primaryStage; // Main stage from "start"
    /*Fileds for main controller*/
      /*All FXML elements on the current scene*/
    @FXML
    private AnchorPane root;
    @FXML
    private BorderPane broot;
    @FXML
    private ImageView iconVisualiser;
    @FXML
    private HBox hmenu;
    @FXML
    public MenuBar menuBar;

    /*Getting the stage from main*/
    public static void setStage(Stage getStage) {
        primaryStage = getStage;
    }

    /*Changing the scene on the main stage*/
    public void changeScene(int num){
        root.getChildren().remove(broot);
        switch (num) {
            case (1):
                try {
                    root.getChildren().addAll((Node) FXMLLoader.load(new File(Resources.FXML.ONE_PLAYER).toURI().toURL()));
                    primaryStage.setWidth(Resources.ScreenSizes.ONE_PLAYER_WIDTH);
                    primaryStage.setHeight(Resources.ScreenSizes.ONE_PLAYER_HEIGHT);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case (2):
                try {
                    root.getChildren().addAll((Node) FXMLLoader.load(new File(Resources.FXML.TWO_PLAYERS).toURI().toURL()));
                    primaryStage.setWidth(Resources.ScreenSizes.TWO_PLAYERS_WIDTH);
                    primaryStage.setHeight(Resources.ScreenSizes.TWO_PLAYERS_HEIGHT);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    /*Overriding method initialize because of implementing "Initializable" interface*/
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
    }

    public void handleFilePlay() {
        if (!isGamePlayingNow) {
             chooseNumPlayers();
        }
    }

    public void chooseNumPlayers() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Приступим!");
        alert.setHeaderText("Вы можете выбрать соперника:");
        alert.setContentText(null);

        ButtonType buttonTypeOne = new ButtonType("Играть с компьютером");
        ButtonType buttonTypeTwo = new ButtonType("Играть с другом");
        ButtonType buttonTypeCancel = new ButtonType("Уйти", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne) {
            changeScene(1);
        } else if (result.get() == buttonTypeTwo) {
            changeScene(2);
        } else {
            alert.close();
        }
    }

    public void handleHelpAboutGame() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Об игре");
        alert.setContentText(" Каждый из игроков задумывает и записывает тайное 4-значное число с неповторяющимися цифрами. Игрок, который начинает игру по жребию, делает первую попытку отгадать число. Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе (то есть количество коров) и сколько угадано вплоть до позиции в тайном числе (то есть количество быков)");
        alert.showAndWait();
    }
    public void handleHelpAboutProgram() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("О программе");
        alert.setHeaderText("Быки и коровы");
        alert.setContentText("Быки и коровы \n Сизов Павел ИВТ-21");
        alert.showAndWait();

    }

    public void handleFileExit() {
        primaryStage.close();
    }

    public void handleFileMainMenu(ActionEvent actionEvent) {
        if(isGamePlayingNow)
        BasePlayerController.exitToMenu();
    }
}