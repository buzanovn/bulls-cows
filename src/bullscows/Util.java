package bullscows;

public class Util {
    public static boolean checkNumberMatchesCorrect(String number) {
        if (number != null) {
            if (!number.isEmpty() && number.length() == 4) {
                for (int i = 0; i < 4; i++) {
                    for (int j = i + 1; j < 4; j++) {
                        if (number.charAt(i) == number.charAt(j))
                            return false;
                    }
                }
                return true;
            }
        }
        return false;
    }
}
