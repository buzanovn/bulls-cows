package bullscows;

public class Resources {
    public static class ScreenSizes {
        public static final double MAIN_WIDTH = 300.0;
        public static final double MAIN_HEIGHT = 445.0;
        public static final double ONE_PLAYER_WIDTH = 360.0;
        public static final double ONE_PLAYER_HEIGHT = 450.0;
        public static final double TWO_PLAYERS_WIDTH = 530.0;
        public static final double TWO_PLAYERS_HEIGHT = 470.0;
    }

    public static class FXML {
        public static final String MAIN = "src/bullscows/view/main_layout.fxml";
        public static final String ONE_PLAYER = "src/bullscows/view/one_player_layout.fxml";
        public static final String TWO_PLAYERS = "src/bullscows/view/two_players_layout.fxml";
    }

    public static final String ICON = "view/icon.png";
}
