package bullscows.model;

public class BullsCows {
    private String bc;

    public BullsCows() {
        this.bc = "";
        int j = 0;
        boolean[] var = new boolean[10];
        int j_0 = (int) (Math.random() * 9 + 1);
        this.bc += j_0;
        var[j_0] = true;

        for (int i = 0; this.bc.length() < 4; i++) {
            j = (int) (Math.random() * 10);
            if (!var[j]) {
                this.bc += j;
                var[j] = true;
            }
        }
    }

    public BullsCows(String string) {
        this.bc = string;
    }

    public BullsCows(Integer integer) {
        this.bc = integer.toString();
    }

    public String getBullsCows() {
        return this.bc;
    }

    public int getBullsCount(BullsCows bc_internal) {
        int bulls = 0;
        for (int i = 0; i < 4; i++) {
            if (this.bc.charAt(i) == bc_internal.getBullsCows().charAt(i))
                bulls++;
        }
        return bulls;
    }

    public int getCowsCount(BullsCows bc_internal) {
        int cows = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (this.bc.charAt(i) == bc_internal.getBullsCows().charAt(j) && i != j)
                    cows++;
            }
        }
        return cows;
    }

    public String toString(BullsCows bc_internal) {
        return String.format("%1sб, %2sк", getBullsCount(bc_internal), getCowsCount(bc_internal));
    }

}

