/*
 * Copyright (c) 2017. All rights reserved.
 */

package bullscows.model;

import javafx.beans.NamedArg;

public class GuessResult {
    private String guessNum;
    private String guessResult;

    public GuessResult(String num, String result){
        this.guessNum = num;
        this.guessResult = result;
    }

    public String getGuessNum() {
        return guessNum;
    }

    public String getGuessResult() {
        return guessResult;
    }
}
