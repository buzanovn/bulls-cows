/*
 * Copyright (c) 2017. All rights reserved.
 */

package bullscows;

import bullscows.controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.net.MalformedURLException;

public class Main extends Application {
    /* Main Window */
    public Stage mainWindow;
    /*Main scene*/
    public static Scene mainScene;
    /*First scene for 1-player game */
    public static Scene onePlayerScene;
    /*Second scene for 2-player game */
    public static Scene twoPlayerScene;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws MalformedURLException {
        mainWindow = primaryStage;
        mainWindow.setTitle("Bulls and Cows");
        mainWindow.getIcons().add(new Image(getClass().getResourceAsStream(Resources.ICON)));
        FXMLLoader loader = new FXMLLoader(new File(Resources.FXML.MAIN).toURI().toURL());
        try {
            Controller mainStageController = loader.getController();
            mainStageController.setStage(primaryStage);
            mainScene = new Scene(loader.load());
            mainWindow.setScene(mainScene);
            mainWindow.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
